'use strict';

/**
 * Replaces single quotes with double quotes in selectors and attribute selectors in Sass files. 
**/

module.exports = function (gb) {
    gb.gulp.task('sass-quotes', ['sass-braces', 'sass-trailing-whitespace'], function() {
        var replace = require('gulp-replace'),
            sassFiles = [gb.workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/compiled/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/**/compiled/*.s+(a|c)ss'];

        return gb.gulp.src(sassFiles, {base: './'})
            .pipe(replace(/'.*';|\[.*='.*'\]/g, function(match) {
                //need replace twice to change both quotes
                var replacement = match.replace("'", '"').replace("'", '"');
                return replacement;
            }))
            .pipe(gb.gulp.dest('./'));
    });
};
