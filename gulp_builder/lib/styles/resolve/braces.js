'use strict';

/**
 * Detects "{" in Sass files with too many spaces or no spaces before it and adds a single space.
 */

module.exports = function (gb) {
    gb.gulp.task('sass-braces', function() {
        var replace = require('gulp-replace'),
            sassFiles = [gb.workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/compiled/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/**/compiled/*.s+(a|c)ss'];
        return gb.gulp.src(sassFiles, {base: './'})
            .pipe(replace(/[a-zA-Z0-9\(\)}]+( [\ ]+|){/g, function(match) {
                var replacement;
                //Checks if brace spacing error after interlopation, needs a different replace
                if (/[}]( [\ ]+|){/.test(match)) {
                    console.log('here');
                    //Not sure why need 2 spaces but its the only way I can find to add a SINGLE whitespace before the brace. Look into this
                    replacement = match.replace(/({| [\ ]+{)/g, '  {');
                    console.log(replacement);
                } else {
                    replacement = match.replace(/( [\ ]+|){/g, ' {');
                }
                return replacement;
            }))
            .pipe(gb.gulp.dest('./'));
    });
};
