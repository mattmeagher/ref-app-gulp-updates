'use strict';

/**
 * Looks for and removes trailing whitespace following a brace or semicolon in Sass files
**/

module.exports = function (gb) {
    gb.gulp.task('sass-trailing-whitespace', ['sass-braces'], function() {
        var replace = require('gulp-replace'),
            sassFiles = [gb.workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/compiled/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/**/compiled/*.s+(a|c)ss'];

             return gb.gulp.src(sassFiles, {base: './'})
                 .pipe(replace(/} {|} |{ |; /g, function(match) {
                     //Don't replace if selector uses interlopation
                     if (match === "} {") {
                         return match;
                     }
                     let replacement = match.slice(0, -1);
                     return replacement;

                 }))
                 .pipe(gb.gulp.dest('./'));

    });
};
