'use strict';

/**
 * Converts hard tabs into 4 spaces in Sass files
**/

module.exports = function (gb) {
    gb.gulp.task('sass-tabs-to-spaces', ['sass-braces', 'sass-trailing-whitespace', 'sass-quotes'], function() {
        var soften = require('gulp-soften'),
        sassFiles = [gb.workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/compiled/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/**/compiled/*.s+(a|c)ss'];
        return gb.gulp.src(sassFiles, {base: './'})
            .pipe(soften(4))
            .pipe(gb.gulp.dest('./'));
    });
};
