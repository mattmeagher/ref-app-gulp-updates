'use strict';

/**
 * Adds a space after : if one doesn't exist in sass files
**/

module.exports = function (gb) {
    gb.gulp.task('sass-colon', ['sass-braces', 'sass-trailing-whitespace'], function() {
        var replace = require('gulp-replace'),
            sassFiles = [gb.workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/compiled/*.s+(a|c)ss',
                     '!' + gb.workingPath + '/../**/cartridge/scss/**/compiled/*.s+(a|c)ss'];

        return gb.gulp.src(sassFiles, {base: './'})
            .pipe(replace(/[a-z]+:[$a-zA-Z0-9]/g, function(match) {
                var replacement = match.replace(':', ': ');

                return replacement;
            }))
            .pipe(gb.gulp.dest('./'));
    });
};
