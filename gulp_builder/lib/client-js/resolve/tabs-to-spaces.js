'use strict';

/**
 * Converts hard tabs into 4 spaces in client JavaScript files
**/

module.exports = function (gb) {
    gb.gulp.task('js-tabs-to-spaces', function() {
        var soften = require('gulp-soften');

        return gb.gulp.src(gb.allJavascriptFiles, {base: './'})
            .pipe(soften(4))
            .pipe(gb.gulp.dest('./'));
    });
};
